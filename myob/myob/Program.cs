﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myob
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            string payperiod;
            double grossincome = 0;
            double incometax = 0;
            double netincome = 0;

            double super = 0;

            Console.WriteLine("Employee Monthly Payslip");
            string[] lines = File.ReadAllLines(@"Data.txt", Encoding.UTF8);
            foreach (var line in lines)
            {
                string[] ar = line.Split(',');
                name = ar[0];
                payperiod = ar[1];
                grossincome = Convert.ToDouble(ar[2]);
                incometax = Convert.ToDouble(ar[3]);
                netincome = Convert.ToDouble(ar[4]);
                super = Convert.ToDouble(ar[5]);
            }
            using (StreamWriter writer = new StreamWriter(@"Data.txt", true))
            {
                writer.WriteLine("  ");
                writer.WriteLine("  ");
            }
            Console.ReadLine();
        }
    }
}
